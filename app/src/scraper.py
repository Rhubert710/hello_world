
from selenium.webdriver import Chrome
from selenium import webdriver 
from selenium.webdriver.chrome.options import Options
import pandas as pd
import sqlite3

chrome_options = Options()
chrome_options.add_argument("--headless")
driver = webdriver.Chrome("/Users/rh/Desktop/Py/it/pytest/pytest-tutorial/automation/chromedriver", options=chrome_options)

pages_to_scrape = 20

URL = "https://www.xda-developers.com/"

scrapd_data_array = []
for page in range(1,pages_to_scrape+1):

    driver.get(URL)
    
    sections = driver.find_elements_by_class_name("layout_post_2")

    for section in sections:

        articles = section.find_elements_by_class_name('item_content')
        
        for article in articles:


            headline = str(article.find_element_by_tag_name('h4').text).replace(',', '')

            excerpt = str(article.find_element_by_class_name('the-excerpt').text).replace(',', '')

            author = article.find_element_by_class_name('meta_author').text

            date_posted = "2021-" + str(article.find_element_by_class_name('meta_date').text)[:-5]
            print(date_posted)


            scrapd_data_array.append([headline,excerpt,author,date_posted])

            # print (scraped_array)

        # print(driver.find_element_by_class_name('next').text)
        URL = f'https://www.xda-developers.com/page/{page+1}/'

driver.close()


df = pd.DataFrame(scrapd_data_array,columns=['Headline','Excerpt','Author','Date_Posted'])
df.to_csv('XDA_selenium_scrape.csv')


'''
write to xda_data.db
'''
conn = sqlite3.connect('xda_data.db')
c = conn.cursor()

df.to_sql('xda_data', conn, if_exists='replace', index = False)

# a = c.execute('''SELECT * FROM xda_data''').fetchall()
# print(a)

