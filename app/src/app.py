from flask import Flask, jsonify, request
import sqlite3
import json

app = Flask(__name__)

@app.route('/')
def hello_world():
    return flask.render_template('index.html')

@app.route('/api')
def rest_hello_world():
    return '{"id":0,"message":"Flask: Hello World from Docker"}'

@app.get('/data')
def get_all_data():

    conn = sqlite3.connect('xda_data.db')
    c = conn.cursor()

    data = c.execute('''SELECT * FROM xda_data''').fetchall()

    return json.dumps(data)

@app.get('/data/<limit_arg>')
def get_data_limit(limit_arg):
    conn = sqlite3.connect('xda_data.db')
    c = conn.cursor()

        
    if  limit_arg.isdigit():

        data = c.execute(f'''SELECT * FROM xda_data LIMIT {limit_arg}''').fetchall()
        return jsonify(data)

    if limit_arg =='*':

        data = c.execute(f'''SELECT * FROM xda_data''').fetchall()
        return jsonify(data)

    else:
        return 'error: number of results must be a interger'

@app.get('/data/<limit_arg>/<colums_arg>')
def get_data_colums(limit_arg, colums_arg):
    conn = sqlite3.connect('xda_data.db')
    c = conn.cursor()

    col =[]

    if colums_arg == '*':
        col.append('*')
    else:
        
        if 'h' in colums_arg.lower():
            col.append('headline')
        if 'e' in colums_arg.lower():
            col.append('excerpt')
        if 'a' in colums_arg.lower():
            col.append('author')
        if 'd' in colums_arg.lower():
            col.append('date_posted')
        if ('h' not in colums_arg.lower())and('e' not in colums_arg.lower())and('a' not in colums_arg.lower())and('d' not in colums_arg.lower()):
            return 'error: must include h,e,a or d'

    col_str = col_str=str(col).replace("'", "")
    col_str = col_str.replace('[', "")
    col_str = col_str.replace(']', "")
 
    if  limit_arg.isdigit():

        data = c.execute(f'''SELECT {col_str} FROM xda_data LIMIT {limit_arg}''').fetchall()
        return jsonify(data)

    if limit_arg =='*':

        data = c.execute(f'''SELECT {col_str} FROM xda_data''').fetchall()
        return jsonify(data)

    else:
        return 'error: number of results must be an integer'


@app.get('/data/<limit_arg>/<colums_arg>/<from_date>')
def get_data_from_date(limit_arg, colums_arg, from_date):
    conn = sqlite3.connect('xda_data.db')
    c = conn.cursor()


    col =[]

    if colums_arg == '*':
        col.append('*')
    else:
        
        if 'h' in colums_arg.lower():
            col.append('headline')
        if 'e' in colums_arg.lower():
            col.append('excerpt')
        if 'a' in colums_arg.lower():
            col.append('author')
        if 'd' in colums_arg.lower():
            col.append('date_posted')
        if ('h' not in colums_arg.lower())and('e' not in colums_arg.lower())and('a' not in colums_arg.lower())and('d' not in colums_arg.lower()):
            return 'error: must include h,e,a or d'

    col_str = col_str=str(col).replace("'", "")
    col_str = col_str.replace('[', "")
    col_str = col_str.replace(']', "")



    if  limit_arg.isdigit():
    
        data = c.execute(f'''SELECT {col_str} FROM xda_data WHERE date_posted >= "{from_date}" LIMIT {limit_arg} ''').fetchall()
        return jsonify(data)

    if limit_arg =='*':

        data = c.execute(f'''SELECT {col_str} FROM xda_data WHERE date_posted >= "{from_date}"''').fetchall()
        return jsonify(data)

    else:
        return 'error: number of results must be an integer'



@app.get('/data/<limit_arg>/<colums_arg>/<from_date>/<to_date>')
def get_data_to_date(limit_arg, colums_arg, from_date, to_date):
    conn = sqlite3.connect('xda_data.db')
    c = conn.cursor()


    col =[]

    if colums_arg == '*':
        col.append('*')
    else:
        
        if 'h' in colums_arg.lower():
            col.append('headline')
        if 'e' in colums_arg.lower():
            col.append('excerpt')
        if 'a' in colums_arg.lower():
            col.append('author')
        if 'd' in colums_arg.lower():
            col.append('date_posted')
        if ('h' not in colums_arg.lower())and('e' not in colums_arg.lower())and('a' not in colums_arg.lower())and('d' not in colums_arg.lower()):
            return 'error: must include h,e,a or d'

    col_str = col_str=str(col).replace("'", "")
    col_str = col_str.replace('[', "")
    col_str = col_str.replace(']', "")



    if  limit_arg.isdigit():
    
        data = c.execute(f'''SELECT {col_str} FROM xda_data WHERE date_posted >= "{from_date}" LIMIT {limit_arg} ''').fetchall()
        return jsonify(data)

    if limit_arg =='*':

        data = c.execute(f'''SELECT {col_str} FROM xda_data WHERE date_posted >= "{from_date}" and date_posted <= "{to_date}"''').fetchall()
        return jsonify(data)

    else:
        return 'error: number of results must be an integer'



if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
